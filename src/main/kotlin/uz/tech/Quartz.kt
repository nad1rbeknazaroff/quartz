package uz.tech

import org.quartz.*
import org.springframework.context.ApplicationContext
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.scheduling.quartz.SchedulerFactoryBean
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*
import javax.annotation.PostConstruct
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

// Interface defining a base job with trigger and jobDetail methods
interface BaseJob : Job {
    fun trigger(): Trigger
    fun jobDetail(): JobDetail
}

// EmailJob class implementing BaseJob interface
@Component
class EmailJob(
    private val quartzLogRepository: QuartzLogRepository
) : BaseJob {

    // Execute method that logs information and prints a message
    override fun execute(context: JobExecutionContext?) {
        quartzLogRepository.save(QuartzLog("Email"))
        println("Time: ${LocalDateTime.now()} /// Email sent successfully! ///")
    }

    // Trigger method for scheduling the job with a simple schedule
    override fun trigger(): Trigger = TriggerBuilder.newTrigger()
        .forJob(jobDetail())
        .withIdentity("EMAIL_SENT_TRIGGER")
        .withSchedule(
            SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(45).repeatForever()
        )
        .withDescription("Email sending Job trigger.")
        .build()

    // JobDetail method for creating the job detail
    override fun jobDetail(): JobDetail = JobBuilder.newJob(javaClass)
        .withIdentity("EMAIL_SENT")
        .withDescription("Email Sent Job.")
        .storeDurably()
        .build()
}

// NotificationJob class implementing BaseJob interface
@Component
class NotificationJob(
    private val quartzLogRepository: QuartzLogRepository
) : BaseJob {

    // Execute method that logs information and prints a message
    override fun execute(context: JobExecutionContext?) {
        quartzLogRepository.save(QuartzLog("Notification"))
        println("Time: ${LocalDateTime.now()} /// Notification sent successfully! ///")
    }

    // Trigger method for scheduling the job with a cron expression
    override fun trigger(): Trigger = TriggerBuilder.newTrigger()
        .forJob(jobDetail())
        .withIdentity("Notification_SENT_TRIGGER")
        .withSchedule(CronScheduleBuilder.cronSchedule("0 * * ? * *")) // Every minute
        .withDescription("Notification sending Job trigger.")
        .build()

    // JobDetail method for creating the job detail
    override fun jobDetail(): JobDetail = JobBuilder.newJob(javaClass)
        .withIdentity("NOTIFICATION_SENT")
        .withDescription("Notification Sent Job.")
        .storeDurably()
        .build()
}

// QuartzSchedulerService for scheduling jobs during application startup
@Service
class QuartzSchedulerService(
    private val applicationContext: ApplicationContext,
    private val schedulerFactoryBean: SchedulerFactoryBean
) {

    // PostConstruct method to schedule jobs during application startup
    @PostConstruct
    fun scheduleJobs() {
        val scheduler: Scheduler = schedulerFactoryBean.scheduler

        // Get all beans implementing BaseJob
        val quartzConfigBeans = applicationContext.getBeansOfType(BaseJob::class.java)

        // Schedule jobs for each BaseJob implementation
        quartzConfigBeans.values.forEach { baseJob ->
            val jobDetail = baseJob.jobDetail()
            val trigger = baseJob.trigger()

            // Check if the job is not already scheduled to avoid duplication
            if (scheduler.getJobDetail(JobKey(jobDetail.key.name)) == null) {
                scheduler.scheduleJob(jobDetail, trigger)
            }
        }
    }
}

// Entity class representing Quartz job execution logs
@Entity
class QuartzLog(
    val job: String,
    val time: Date = Date(),
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null
)

// Repository interface for QuartzLog entity
@Repository
interface QuartzLogRepository : JpaRepository<QuartzLog, Long>


