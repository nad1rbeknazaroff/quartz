//package uz.tech.springboot
//
//import org.quartz.*
//import org.quartz.impl.matchers.GroupMatcher
//import org.quartz.impl.matchers.NameMatcher
//import org.springframework.context.annotation.Bean
//import org.springframework.stereotype.Component
//import java.time.LocalDateTime
//import java.util.*
//
//
//class JobType2 : Job {
//    @Throws(JobExecutionException::class)
//    override fun execute(context: JobExecutionContext?) {
//        println(LocalDateTime.now())
//        schedule()
//    }
//}
////
////class JobType1 : Job {
////    @Throws(JobExecutionException::class)
////    override fun execute(context: JobExecutionContext?) {
////        println(LocalDateTime.now())
////        println("/////////////////////////////////////")
////    }
////}
//
//fun schedule() {
//    println("Job executed successfully!")
//}
//
//@Component
//class JobType2Config(private val scheduler: Scheduler) {
//
//    @Bean
//    fun jobType2Detail() {
//        val jobKeyList = scheduler.getJobDetail(JobKey("SENT_EPC_DOCUMENT"))
//        println(jobKeyList)
//
//        if (jobKeyList == null) {
//            val jobDetail: JobDetail = JobBuilder.newJob(JobType2::class.java)
//                .withIdentity("SENT_EPC_DOCUMENT")
//                .withDescription("Printing Job.")
//                .storeDurably()
//                .build()
//
//            val trigger = TriggerBuilder.newTrigger()
//                .forJob(jobDetail)
//                .withIdentity("SENT_EPC_DOCUMENT")
//                .withSchedule(
////                CronScheduleBuilder.cronSchedule(/**/"0 0 22 * * ?")
//                    SimpleScheduleBuilder.simpleSchedule().withIntervalInMilliseconds(50).repeatForever()
//                )
//                .withDescription("Simple message printing Job trigger.")
//                .build()
//
//            scheduler.scheduleJob(jobDetail, trigger)
//        }
//    }
////
////    @Bean
////    fun jobType1Detail() {
////        val jobKeyList = scheduler.getJobKeys(GroupMatcher.groupEquals("USER"))
////        println(jobKeyList)
////        if (jobKeyList.size == 0) {
////            val jobDetail: JobDetail = JobBuilder.newJob(JobType1::class.java)
////                .withIdentity("BALANCE", "USER")
////                .withDescription("Printing Job.")
////                .storeDurably()
////                .build()
////
////            val trigger = TriggerBuilder.newTrigger()
////                .forJob(jobDetail)
////                .withIdentity("BALANCE")
////                .withSchedule(
//////                CronScheduleBuilder.cronSchedule("0 10 17 * * ?")
////                    SimpleScheduleBuilder.simpleSchedule().withIntervalInMilliseconds(50).repeatForever()
////                )
////                .withDescription("Simple message printing Job trigger.")
////                .build()
////
////            scheduler.scheduleJob(jobDetail, trigger)
////        }
//    /*}*/
//
//
//}
